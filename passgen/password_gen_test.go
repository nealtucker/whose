package passgen

import "testing"

func TestPasswords(t *testing.T) {
	secret := "hello there"
	spec := PasswordSpec{
		Length: 20,
	}
	pass, err := GeneratePassword(secret, "password1", spec)
	if err != nil {
		t.Fatalf("couldn't generate password from secret: %v", err)
	}
	if pass != "$[;@`MBinFHjyXWrwgd3" {
		t.Fatal("password value doesn't match expected")
	}
	t.Logf("Generated password: %q", pass)

	spec = PasswordSpec{
		Length:  20,
		Upper:   2,
		Lower:   2,
		Digit:   5,
		Special: 3,
	}
	pass, err = GeneratePassword(secret, "password1", spec)
	if err != nil {
		t.Fatalf("couldn't generate password from secret: %v", err)
	}
	if pass != "{0E@22Bqn^H5y9WrIs~3" {
		t.Fatal("password value doesn't match expected")
	}

	t.Logf("Generated password: %q", pass)
	spec = PasswordSpec{
		Length:         20,
		Upper:          2,
		Lower:          2,
		Digit:          5,
		Special:        3,
		AllowedSpecial: "@#$",
	}
	pass, err = GeneratePassword(secret, "password1", spec)
	if err != nil {
		t.Fatalf("couldn't generate password from secret: %v", err)
	}
	if pass != "#0Eb22lqw#U5y9OQIs$D" {
		t.Fatal("password value doesn't match expected")
	}
	t.Logf("Generated password: %q", pass)
	spec = PasswordSpec{
		Length:         20,
		Upper:          2,
		Lower:          2,
		Digit:          5,
		Special:        3,
		AllowedSpecial: "^&*",
	}
	pass, err = GeneratePassword(secret, "password1", spec)
	if err != nil {
		t.Fatalf("couldn't generate password from secret: %v", err)
	}
	if pass != "&0Eb22lqw&U5y9OQIs*D" {
		t.Fatal("password value doesn't match expected")
	}
	t.Logf("Generated password: %q", pass)
}
