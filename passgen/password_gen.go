package passgen

import (
	"fmt"
	"io"
)

// PasswordSpec - interface stolen straight from cloudflare/gokey
type PasswordSpec struct {
	Length         int
	Upper          int
	Lower          int
	Digits         int
	Special        int
	AllowedSpecial string
}

// GeneratePassword - deterministically generate a password based on
// 'spec' parameters and the given key, much like gokey does, but do it in
// constant time.  I found that gokey's approach of generating a password and
// then determining whether or not it met the requirements did not actually
// terminate in a reasonable time with some of the password requirements I
// handed it.  This approach is different in that it selects the necessary
// characters from all character classes, then randomizes the order of the
// characters it selected.  It should finish in constant time.  It is a bit
// wasteful in that it eats two bytes of hash data per character needed,
// plus some bytes to do the randomization, but given the SHAGenerator,
// we have unlimited bytes and this is not very expensive.
//
// Why two bytes per needed character?  Glad you asked.  We could just grab
// the number of bits needed to select the next character from the given
// character "class" (e.g. uppercase) and that would be very efficient,
// but just doing a modulo operation means the passwords get weakened by the
// lopsidedness of doing e.g. 32%26 (selecting 5 bits of entropy to choose
// from 1 of 26 chars results in some values being selected 3.1% of the
// time and some values being selected 6.3% of the time).  Doing 65536%26
// reduces the lopsidedness considerably (3.845% vs 3.847%), and while I
// wouldn't use this for actual cryptography, it seems quite acceptable
// for password generation.  Note that as the size of the various character
// classes gets to be significantly large compared to 65536, this assumption
// starts to get worse and worse, but the only unbounded character class
// here is the user-defined "special" class, and if you define one with 65k
// characters in it, you get what you deserve, which is passwords that are
// slightly more vulnerable to cryptanalysis.
func GeneratePassword(passphrase string, identifier string, spec PasswordSpec) (result string, err error) {
	seed := []byte(fmt.Sprintf("%s %s", passphrase, identifier))
	generator, err := NewSHAGenerator(seed)
	if err != nil {
		return
	}

	var required []byte
	var randish uint16

	// get the required number of uppercases
	chosenUppers, err := selectCharsFromClass(uppers, spec.Upper, generator)
	if err != nil {
		return
	}
	required = append(required, chosenUppers...)

	// get the required number of lowercases
	chosenLowers, err := selectCharsFromClass(lowers, spec.Lower, generator)
	if err != nil {
		return
	}
	required = append(required, chosenLowers...)

	// get the required number of digits
	chosenDigits, err := selectCharsFromClass(digits, spec.Digits, generator)
	if err != nil {
		return
	}
	required = append(required, chosenDigits...)

	// get the required number of specials, using the passed-in special string if appropriate
	specialChars := standardSpecials
	if spec.AllowedSpecial != "" {
		specialChars = []byte(spec.AllowedSpecial)
	}
	chosenSpecial, err := selectCharsFromClass(specialChars, spec.Special, generator)
	if err != nil {
		return
	}
	required = append(required, chosenSpecial...)

	// select remaining chars from a combination of all previous classes
	allChars := combineClasses([][]byte{uppers, lowers, digits, specialChars})
	if len(required) < spec.Length {
		var chosenAll []byte
		chosenAll, err = selectCharsFromClass(allChars, spec.Length-len(required), generator)
		if err != nil {
			return
		}
		required = append(required, chosenAll...)
	}

	// deterministically swizzle the order of the selected bytes
	var output []byte
	for len(required) > 0 {
		randish, err = next16(generator)
		if err != nil {
			return
		}
		// the "class" now is the array of all selected bytes from above
		index := selectFrom(required, randish)
		output = append(output, required[index])

		// remove appended byte from 'required' array, shifting everything else down
		copy(required[index:], required[index+1:])
		required = required[:len(required)-1]
	}

	result = string(output)
	return
}

// get n bytes from the given class, using the deterministic generator gen
func selectCharsFromClass(class []byte, n int, gen *SHAGenerator) (result []byte, err error) {
	var randish uint16
	for i := 0; i < n; i++ {
		randish, err = next16(gen)
		if err != nil {
			return
		}
		index := selectFrom(class, randish)
		result = append(result, class[index])
	}
	return
}

// selectFrom - given a candidate byte array and a uint16, return the
// corresponding index in the range of the candidate array.  This is the
// source of the lopsidedness and the reason we're wasting bytes by using uint16
func selectFrom(candidates []byte, nextVal uint16) uint16 {
	if len(candidates) > 65535 {
		panic("candidate character class cannot have 65536 members")
	}
	return nextVal % (uint16(len(candidates)))
}

// next16 - grab a uint16 using the next two bytes from the reader
func next16(src io.Reader) (result uint16, err error) {
	var data [2]byte
	n, err := src.Read(data[:])
	if err != nil {
		return
	}
	if n != 2 {
		err = fmt.Errorf("could not read 2 random bytes from shagenerator which is very sad and broken")
		return
	}
	result = uint16(data[0]) | (uint16(data[1]) << 8)
	return
}

// classes this generator cares about.  standardSpecials can be overridden in a password spec
var uppers = []byte("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
var lowers = []byte("abcdefghijklmnopqrstuvwxyz")
var digits = []byte("0123456789")
var standardSpecials = []byte("`~!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?")

// concatenate an array of byte arrays into a single byte array
func combineClasses(classes [][]byte) []byte {
	var result []byte
	for _, class := range classes {
		result = append(result, class...)
	}
	return result
}
