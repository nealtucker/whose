package passgen

import "testing"

func TestSHA(t *testing.T) {
	gen, err := NewSHAGenerator([]byte("testing"))
	if err != nil {
		t.Fatalf("failed to create sha generator %v", err)
	}
	var buf = make([]byte, 2)
	for i := 0; i < 200; i++ {
		n, err := gen.Read(buf)
		if err != nil {
			t.Errorf("failed to read from gen: %v", err)
			break
		}
		t.Logf("read %d bytes from gen: %v", n, buf)
	}
}
