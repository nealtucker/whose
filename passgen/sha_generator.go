package passgen

import (
	"bytes"
	"crypto/sha256"
	"fmt"
)

// SHAGenerator - a stateful pseudorandom generator which emits bytes based on an initial
// seed being fed to a SHA256 hash along with a counter which increases as the buffer runs
// out.  Implements io.Reader
type SHAGenerator struct {
	availableBytes []byte
	nextRound      int
	initialKey     []byte
}

// NewSHAGenerator - create a SHAGenerator using the given buffer as the initial seed
func NewSHAGenerator(key []byte) (result *SHAGenerator, err error) {
	dg := SHAGenerator{
		initialKey: key,
	}
	err = dg.fillBuf()
	if err != nil {
		return
	}
	result = &dg
	return
}

// fillBuf - do one more iteration of SHA by bumping nextRound and combining it with initialKey
// one more time.  Append the result to the available bytes.
func (sg *SHAGenerator) fillBuf() (err error) {
	buf := bytes.Buffer{}
	_, err = buf.Write(sg.initialKey)
	if err != nil {
		return
	}
	fmt.Fprintf(&buf, "%d", sg.nextRound)
	sg.nextRound++
	sha := sha256.Sum256(buf.Bytes())
	sg.availableBytes = append(sg.availableBytes, sha[:]...)
	return
}

// Read - return the next chunk of bytes.  Note that if the
// available buffer is short, another chunk will be read, but
// only one.  If you request more bytes than can be fullfilled
// with a single fillBuf() call, you will get a short result
// and need to call again.  This will never happen in this
// application.
func (sg *SHAGenerator) Read(p []byte) (n int, err error) {
	if len(sg.availableBytes) < len(p) {
		err = sg.fillBuf()
		if err != nil {
			return
		}
	}

	n = copy(p, sg.availableBytes)
	sg.availableBytes = sg.availableBytes[n:]
	return
}
