package whose

import "testing"

func TestLoadData(t *testing.T) {
	accounts, err := ReadAccounts("./testdata/accounts.json")
	if err != nil {
		t.Errorf("Failed to load conf: %v", err)
	}
	for k := range accounts.ToArray() {
		t.Logf("account: %q", k)
	}
	_ = accounts
}

//fixme: add tests for cases like:
// length specified/unspecified
// password constraints specified/partiallyspecified
