package whose

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"sort"
	"strings"
	"time"

	"bitbucket.org/nealtucker/whose/util"

	"bitbucket.org/nealtucker/whose/passgen"
)

// AccountInfo - the records found in AccountsData
type AccountInfo struct {
	Name     string        `json:"-"` // note: filled in with parent key for ease of use
	Version  int           `json:"version,omitempty"`
	Length   int           `json:"length,omitempty"`
	Note     string        `json:"note,omitempty"`
	Username string        `json:"username,omitempty"`
	Spec     *PasswordSpec `json:"password_spec,omitempty"`
}

// PasswordSpec - optional AccountInfo field which constrains password gen
type PasswordSpec struct {
	Upper        int    `json:"min_uppercase,omitempty"`
	Lower        int    `json:"min_lowercase,omitempty"`
	Digits       int    `json:"min_digits,omitempty"`
	Special      int    `json:"min_special,omitempty"`
	SpecialChars string `json:"allowed_special,omitempty"`
}

var sampleAccounts = []AccountInfo{
	{
		Name: "example.com",
	},
	{
		Name:     "example.com moreoptions",
		Username: "you@gmail.com",
		Version:  3,
		Length:   12,
		Note: `You can put whatever notes you want in your account
info and they will be displayed when selecting
that account.`,
	},
	{
		Name:     "seattlecitylight",
		Username: "you@gmail.com",
		Note: `you can also include a password spec for lame sites
that have special rules.  Note that integer minimums
that are omitted are set to 1. Omitting the
allowed_special_chars field sets it to a reasonable
default set.`,
		Spec: &PasswordSpec{
			Upper:        1,
			Lower:        1,
			Digits:       2,
			Special:      2,
			SpecialChars: "@#$%^&*",
		},
	},
}

// Params - Once the main UI selects an
// AccountInfo to operate on, this function generates the params it passes to
// the password generator to generate the password
func (ai AccountInfo) Params() (realm string, spec passgen.PasswordSpec, err error) {
	// defaults
	spec = passgen.PasswordSpec{
		Length:  20,
		Upper:   1,
		Lower:   1,
		Digits:  1,
		Special: 1,
	}
	if ai.Spec != nil {
		spec.Upper = ai.Spec.Upper
		spec.Lower = ai.Spec.Lower
		spec.Digits = ai.Spec.Digits
		spec.Special = ai.Spec.Special
		if ai.Spec.SpecialChars != "" {
			spec.AllowedSpecial = ai.Spec.SpecialChars
		}
	}
	if ai.Length != 0 {
		spec.Length = ai.Length
	}
	realm = fmt.Sprintf("%s %d", ai.Name, ai.Version)

	return
}

// AccountsData - the structure of the data pointed to by dataPath
type AccountsData map[string]AccountInfo
type AppSettings struct {
	AlwaysDaemonize      bool          `json:"always_daemonize"`
	PassphraseTimeoutCfg string        `json:"passphrase_timeout"`
	PassphraseTimeout    time.Duration `json:"-"` // parsed version
}

type Config struct {
	// TODO: config versioning
	Settings AppSettings  `json:"settings"`
	Accounts AccountsData `json:"accounts"`
}

// ToArray - convert the map to an array of structs
func (ad AccountsData) ToArray() []AccountInfo {
	result := []AccountInfo{}
	for _, ai := range ad {
		result = append(result, ai)
	}
	sort.Sort(ByName(result))
	return result
}

// ByName - sort an account info array by name
type ByName []AccountInfo

func (a ByName) Len() int           { return len(a) }
func (a ByName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByName) Less(i, j int) bool { return strings.Compare(a[i].Name, a[j].Name) <= 0 }

// Note: might be nice to implement a "local to this directory" whose conf
// that walks up your directory tree so you could have different confs for
// different projects.  Food for thought.  This would require moving some
// of the sockpath-type stuff into config rather than just env/args

// ReadConfig - given a path, read the config and return it
// if path is empty, try ${HOME}/.whose/config.json
func ReadConfig(cfgPath string) (result *Config, err error) {
	if cfgPath == "" {
		return ReadConfig(path.Join(os.Getenv("HOME"), ".whose/config.json"))
	}

	bytes, err := ioutil.ReadFile(cfgPath)
	if err != nil {
		return
	}

	cfg := Config{
		Accounts: make(AccountsData),
	}

	err = json.Unmarshal(bytes, &cfg)
	if err != nil {
		return
	}

	// make sure each record has a copy of its key in .Name
	for k := range cfg.Accounts {
		ai := cfg.Accounts[k]
		ai.Name = k
		cfg.Accounts[k] = ai
	}

	if len(cfg.Accounts) == 0 {
		log.Printf("Warning: read config %q and found no accounts", cfgPath)
	}

	if cfg.Settings.PassphraseTimeoutCfg != "" {
		cfg.Settings.PassphraseTimeout, err = util.ParseDuration(cfg.Settings.PassphraseTimeoutCfg)
		if err != nil {
			return
		}
	}
	result = &cfg
	return
}

// WriteSampleConfig - write the sample configs to the given io.Writer
func WriteSampleConfig(out io.Writer) (err error) {
	cfg := Config{
		Settings: AppSettings{
			PassphraseTimeoutCfg: "8h",
			AlwaysDaemonize:      true,
		},
		Accounts: make(AccountsData),
	}
	encoder := json.NewEncoder(out)
	for _, ai := range sampleAccounts {
		cfg.Accounts[ai.Name] = ai
	}
	encoder.SetIndent("", "    ")

	return encoder.Encode(cfg)
}
