package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/nealtucker/whose/passgen"

	"bitbucket.org/nealtucker/whose"
	"bitbucket.org/nealtucker/whose/keyhole"
	"bitbucket.org/nealtucker/whose/util"
	"github.com/manifoldco/promptui"
	"golang.org/x/crypto/ssh/terminal"
)

func startDaemon(sockpath string) (err error) {
	ws, err := keyhole.NewServer(sockpath)
	if err != nil {
		err = fmt.Errorf("failed to create keyhole server: %v", err)
		return
	}

	sm := util.NewSignalMaster()
	inthandler := func(sig os.Signal) {
		fmt.Printf("Got signal %d\n", sig)
		ws.Quit()
	}
	sm.Add(os.Interrupt, inthandler)
	sm.Add(syscall.SIGTERM, inthandler)

	err = ws.Serve()
	if err != nil {
		err = fmt.Errorf("failed to start keyhole server: %v", err)
		return
	}
	return
}

func relaunchAsServer() (err error) {
	exe, err := os.Executable()
	if err != nil {
		return
	}
	cmd := exec.Command(exe,
		"-serve",
		"-sockpath", args.keyholePath)

	return cmd.Start()
}

var args struct {
	initialSearch  string
	keyholePath    string
	configPath     string
	phraseTestPath string
	copyCommand    string
	removePhrase   bool
	daemonize      bool
	serve          bool
	verbose        bool
	init           bool // run in "init" mode: create ~/.whose/config.json if not exist
	printPassword  bool // forces printing to stdout instead of doing external clipboard
}
var defaultSockPath = path.Join(os.Getenv("HOME"), ".whose/keyhole_sock")
var defaultConfigPath = path.Join(os.Getenv("HOME"), ".whose/config.json")
var defaultPhraseTestPath = path.Join(os.Getenv("HOME"), ".whose/passphrase_test")
var errCanceled = fmt.Errorf("canceled")

func init() {
	flag.BoolVar(&args.verbose, "v", false, "verbose")
	flag.BoolVar(&args.init, "init", false, "initialize starter whose config")
	flag.BoolVar(&args.printPassword, "printpass", false, "forces password to stdout")
	flag.BoolVar(&args.daemonize, "d", false, "run subprocess as daemon")
	flag.BoolVar(&args.removePhrase, "r", false, "remove passphrase from daemon and exit")
	flag.BoolVar(&args.serve, "serve", false, "run this process as as keyhole")
	flag.StringVar(&args.keyholePath, "sockpath", "", "path to unix domain socket for keyhole passphrase cache")
	flag.StringVar(&args.configPath, "config", "", "path to config json")
	flag.StringVar(&args.copyCommand, "copycommand", "", "command to run to consume password on stdin")
	flag.StringVar(&args.phraseTestPath, "testphrase", "", "path to encrypted file for testing passphrase")
}

func resolveArgs() {
	if args.keyholePath == "" {
		args.keyholePath = os.Getenv("WHOSE_SOCKPATH")
	}
	if args.keyholePath == "" {
		args.keyholePath = defaultSockPath
	}
	if args.configPath == "" {
		args.configPath = os.Getenv("WHOSE_CONFIGPATH")
	}
	if args.configPath == "" {
		args.configPath = defaultConfigPath
	}
	if args.phraseTestPath == "" {
		args.phraseTestPath = os.Getenv("WHOSE_PASSPHRASETESTPATH")
	}
	if args.phraseTestPath == "" {
		args.phraseTestPath = defaultPhraseTestPath
	}
	if args.serve && args.daemonize {
		fmt.Printf("WARNING: -serve and -d both specified, daemonizing to serve in background")
	}
	if args.copyCommand == "" {
		args.copyCommand = os.Getenv("WHOSE_COPYCOMMAND")
	}
	args.initialSearch = strings.Join(flag.Args(), " ")
}

// this is the key to use to store the passphrase in keyhole
const passphraseKey = "whose_passphrase"

func promptForPassphrase(prompt string) (result string, err error) {
	inputFD := int(os.Stdin.Fd())

	termState, err := terminal.GetState(inputFD)
	if err != nil {
		return
	}
	defer func() {
		// ensure we clean up the term if we get interrupted while prompting
		// hmm.  This does not work.
		terminal.Restore(inputFD, termState)
	}()

	fmt.Fprint(os.Stderr, prompt)
	passBytes, err := terminal.ReadPassword(inputFD)
	if err != nil {
		return
	}
	result = string(passBytes)
	fmt.Fprint(os.Stderr, "\n")
	return
}

func testPassphrase(testFilePath string, passphrase string) (result bool, err error) {
	stat, err := os.Stat(testFilePath)
	if err != nil {
		return
	}
	if stat.IsDir() {
		err = fmt.Errorf("passphrase test path %q must specify a file", testFilePath)
		return
	}
	// load key and check passphrase on it

	err = fmt.Errorf("fixme: implement passphrase testing")
	return
}

func main() {
	var err error
	flag.Parse()
	resolveArgs()

	if !args.verbose {
		log.SetOutput(util.NullLogger())
	}

	if args.init {
		err := doInit()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Init failed: %v\n", err)
		}
		return
	}
	if args.serve {
		log.Printf("Starting daemon")
		err = startDaemon(args.keyholePath)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not start server: %s\n", err.Error())
			os.Exit(1)
		}
		return
	} else if args.daemonize {
		log.Printf("Daemonizing")
		err = relaunchAsServer()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to daemonize: %s\n", err.Error())
			os.Exit(1)
		}
		return
	} else if args.removePhrase {
		log.Printf("Removing passphrase (note: notimpl fixme)")
		// create client, send "delete" command for phrase key
		return
	}

	log.Printf("loading config: %q", args.configPath)
	cfg, err := whose.ReadConfig(args.configPath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to load config: %v\n", err)
		return
	}

	// create client, attempt to retrieve phrase
	var passphrase string
	log.Printf("Creating keyhole client")
	cli, err := keyhole.NewClient(args.keyholePath)
	if err == nil {
		passphrase, err = cli.Get(passphraseKey)
		log.Printf("Passphrase fetch from keyhole err=%v", err)
	}

	if passphrase == "" {
		//prompt for passphrase
		passphrase, err = promptForPassphrase("Enter passphrase: ")
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to get passphrase: %v", err)
			os.Exit(1)
		}

		keyMatches, err := testPassphrase(args.phraseTestPath, passphrase)
		if err == nil {
			// found passphrase test file
			if !keyMatches {
				fmt.Fprintf(os.Stderr, "Incorrect passphrase")
				os.Exit(1)
			}
		} else {
			var passphraseConfirm string
			passphraseConfirm, err = promptForPassphrase("Re-enter passphrase: ")
			if passphraseConfirm != passphrase {
				fmt.Fprintf(os.Stderr, "Passphrases do not match")
				os.Exit(1)
			}
		}
		if cli != nil {
			expiration := time.Time{}
			if cfg.Settings.PassphraseTimeout != 0 {
				expiration = time.Now().Add(cfg.Settings.PassphraseTimeout)
			}
			err = cli.Store(passphraseKey, passphrase, expiration)
			if err != nil {
				log.Printf("Failed to store passphrase in cache: %v", err)
				// swallow this error
				err = nil
			}
		}
	}

	selection, err := showSelectUI(args.initialSearch, &cfg.Accounts)
	if err != nil {
		if err == errCanceled || err.Error() == "^C" {
			return
		}
		fmt.Fprintf(os.Stderr, "unknown error: %v\n", err)
		return
	}

	err = displayChosenItem(passphrase, selection)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to load accounts: %v\n", err)
		return
	}
}

func displayChosenItem(passphrase string, acct whose.AccountInfo) (err error) {
	if acct.Username != "" {
		fmt.Printf("Username: %s\n", acct.Username)
	}

	identifier, spec, err := acct.Params()
	log.Printf("using pw identifier: %q", identifier)
	pass, err := passgen.GeneratePassword(passphrase, identifier, spec)
	outputPassword(pass)
	return
}

func outputPassword(pass string) {
	if args.printPassword {
		fmt.Printf("Password: %s\n", pass)
	} else {
		err := util.StringToClipboard(pass, args.copyCommand)
		if err == util.ErrNoCopyCommand {
			log.Printf("Warning: no copy command found.  Printing password to stdout.")
			fmt.Printf("Password: %s\n", pass)
		} else {
			fmt.Printf("Password copied to clipboard\n")
		}
	}
}

func showSelectUI(searchInput string, accounts *whose.AccountsData) (result whose.AccountInfo, err error) {
	accountArray := accounts.ToArray()
	searcher := func(input string, index int) bool {
		account := accountArray[index]
		return strings.Contains(strings.ToLower(account.Name), input) ||
			strings.Contains(strings.ToLower(account.Username), input) ||
			strings.Contains(strings.ToLower(account.Note), input)
	}

	// if there's an initial input and the result is exactly one match, return it
	if searchInput != "" {
		matchCount := 0
		firstMatch := 0
		for i := range accountArray {
			if searcher(searchInput, i) {
				matchCount++
				if matchCount == 1 {
					firstMatch = i
				}
			}
		}

		if matchCount == 1 {
			result = accountArray[firstMatch]
			return
		}
	}

	templates := &promptui.SelectTemplates{
		Label:    " ",
		Inactive: `{{ .Name | faint}}`,
		Active:   `{{ .Name }}`,
		Selected: "{{ .Name | bold }}",
		Details: `
----- Details -----
Name:     {{ .Name }}
Username: {{ .Username }}
Note:
{{ .Note }}`,
	}

	prompt := promptui.Select{
		Label:             "Type to find account",
		Items:             accountArray,
		Templates:         templates,
		Size:              10,
		Searcher:          searcher,
		StartInSearchMode: true,
		Stdout:            util.NewBellSkipper(os.Stdout),
	}

	i, _, err := prompt.Run()
	if err != nil {
		return
	}

	result = accountArray[i]
	return
}

// doInit - set up a .whose directory and a sample config as a starting point
func doInit() (err error) {
	fmt.Printf("Setting up whose config...\n")
	dirPath := path.Dir(args.configPath)
	dirStat, err := os.Stat(dirPath)
	log.Printf("dirStat result 1: %T/%v/%v", err, err, dirStat)
	if os.IsNotExist(err) {
		log.Printf("creating dir %q", dirPath)
		err = os.Mkdir(dirPath, os.ModePerm)
		if err != nil {
			return
		}
		dirStat, err = os.Stat(dirPath)
	}
	log.Printf("dirStat result: %v/%v", err, dirStat)
	if !dirStat.IsDir() {
		err = fmt.Errorf("Path %q already exists and is not a directory", dirPath)
		return
	}

	_, err = os.Stat(args.configPath)
	if err == nil {
		fmt.Printf("Config %q already exists, skipping creation\n", args.configPath)
		return
	}
	if !os.IsNotExist(err) {
		return
	}

	f, err := os.Create(args.configPath)
	if err != nil {
		return
	}
	defer f.Close()
	fmt.Printf("Writing sample config to %s\n", args.configPath)
	return whose.WriteSampleConfig(f)
}
