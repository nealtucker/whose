package keyhole

import (
	"bytes"
	"encoding/gob"
	"time"
)

type StoreValueMessage struct {
	Key        string
	Value      string
	Expiration time.Time
}
type StoreValueResponse struct {
	Err     string
	Message string
}
type GetValueMessage struct {
	Key string
}
type GetValueResponse struct {
	Value string
	Err   string
}
type DeleteValueMessage struct {
	Key string
}
type DeleteValueResponse struct {
	Err string
}

// I'm sure there's a better way to do this.  A series of serialization/
// deserialization utilites for the three structs above.

func (svm *StoreValueMessage) Enc() (result []byte, err error) {
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	err = enc.Encode(svm)
	if err != nil {
		return
	}
	result = buf.Bytes()
	return
}
func (svm *StoreValueMessage) MessageName() string {
	return "whose_storevalue"
}
func (svm *StoreValueMessage) Dec(data []byte) (err error) {
	dec := gob.NewDecoder(bytes.NewBuffer(data))
	return dec.Decode(svm)
}
func (svr *StoreValueResponse) Enc() (result []byte, err error) {
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	err = enc.Encode(svr)
	if err != nil {
		return
	}
	result = buf.Bytes()
	return
}
func (svr *StoreValueResponse) Dec(data []byte) (err error) {
	dec := gob.NewDecoder(bytes.NewBuffer(data))
	return dec.Decode(svr)
}
func (gvm *GetValueMessage) Enc() (result []byte, err error) {
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	err = enc.Encode(gvm)
	if err != nil {
		return
	}
	result = buf.Bytes()
	return
}
func (gvm *GetValueMessage) MessageName() string {
	return "whose_getvalue"
}
func (gvm *GetValueMessage) Dec(data []byte) (err error) {
	dec := gob.NewDecoder(bytes.NewBuffer(data))
	return dec.Decode(gvm)
}
func (gvr *GetValueResponse) Enc() (result []byte, err error) {
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	err = enc.Encode(gvr)
	if err != nil {
		return
	}
	result = buf.Bytes()
	return
}
func (gvr *GetValueResponse) Dec(data []byte) (err error) {
	dec := gob.NewDecoder(bytes.NewBuffer(data))
	return dec.Decode(gvr)
}
func (dvm *DeleteValueMessage) Enc() (result []byte, err error) {
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	err = enc.Encode(dvm)
	if err != nil {
		return
	}
	result = buf.Bytes()
	return
}
func (dvm *DeleteValueMessage) MessageName() string {
	return "whose_deletevalue"
}
func (dvm *DeleteValueMessage) Dec(data []byte) (err error) {
	dec := gob.NewDecoder(bytes.NewBuffer(data))
	return dec.Decode(dvm)
}
func (dvr *DeleteValueResponse) Enc() (result []byte, err error) {
	buf := bytes.Buffer{}
	enc := gob.NewEncoder(&buf)
	err = enc.Encode(dvr)
	if err != nil {
		return
	}
	result = buf.Bytes()
	return
}
func (dvr *DeleteValueResponse) Dec(data []byte) (err error) {
	dec := gob.NewDecoder(bytes.NewBuffer(data))
	return dec.Decode(dvr)
}
