package keyhole

import (
	"testing"
	"time"
)

func TestStore(t *testing.T) {
	cs, err := NewClientStore()
	if err != nil {
		t.Fatalf("failed to create store: %v", err)
	}
	err = cs.Add("testkey", "testvalue", time.Time{})
	if err != nil {
		t.Errorf("failed to store key")
	}
	err = cs.Add("testkey2", "testvalue2", time.Time{})
	if err != nil {
		t.Errorf("failed to store key")
	}
	val, err := cs.Get("testkey")
	if err != nil || val != "testvalue" {
		t.Errorf("round trip failed, expected %q got %q, err=%v", "testvalue", val, err)
	}
	val, err = cs.Get("testkey2")
	if err != nil || val != "testvalue2" {
		t.Errorf("round trip failed, expected %q got %q, err=%v", "testvalue2", val, err)
	}
	err = cs.Delete("testkey")
	if err != nil {
		t.Errorf("delete failed: %v", err)
	}
	err = cs.Delete("nonexistentkey")
	if err != nil {
		t.Errorf("delete nonexistent key failed: %v", err)
	}

}
