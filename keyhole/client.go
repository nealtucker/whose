package keyhole

import (
	"fmt"
	"log"
	"net"
	"os"
	"time"

	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
)

// Client - an ssh client to a keyhole server via a unix domain socket
type Client struct {
	sockpath string
	client   *ssh.Client
}

// NewClient - constructor for the keyhole client.  Give the path to the
// keyhole server's unix domain socket
// Requires SSH_AUTH_SOCK to be set in the env to talk to SSH agent
func NewClient(sockpath string) (result *Client, err error) {
	c := Client{
		sockpath: sockpath,
	}
	err = c.connect()
	if err != nil {
		log.Printf("failed to connect to ssh server")
		return
	}

	result = &c
	return
}

// Store - stores the value, optionally with an expiration.
// Use the nil value of time.Time for no expiration
func (c *Client) Store(key string, value string, expires time.Time) (err error) {
	req := StoreValueMessage{
		Key:        key,
		Value:      value,
		Expiration: expires,
	}
	buf, err := req.Enc()
	if err != nil {
		return
	}
	success, respBuf, err := c.client.Conn.SendRequest(req.MessageName(), true, buf)
	if !success {
		err = fmt.Errorf("server refused response")
	}
	if err != nil {
		log.Printf("error when sending %q request: %v", req.MessageName(), err)
		return
	}

	resp := StoreValueResponse{}
	err = resp.Dec(respBuf)
	if err != nil {
		return
	}
	if resp.Err != "" {
		err = fmt.Errorf(resp.Err)
	}
	return
}

// Get - get the value for the given key if it exists
func (c *Client) Get(key string) (result string, err error) {
	req := GetValueMessage{
		Key: key,
	}
	buf, err := req.Enc()
	if err != nil {
		return
	}
	success, respBuf, err := c.client.Conn.SendRequest(req.MessageName(), true, buf)
	if !success {
		err = fmt.Errorf("server refused response")
	}
	if err != nil {
		log.Printf("error when sending %q request: %v", req.MessageName(), err)
		return
	}
	resp := GetValueResponse{}
	err = resp.Dec(respBuf)
	if err != nil {
		return
	}
	result = resp.Value
	if resp.Err != "" {
		err = fmt.Errorf(resp.Err)
	}
	return
}

// Delete - delete the given key/value pair from your kv store
func (c *Client) Delete(key string) (result string, err error) {
	req := DeleteValueMessage{
		Key: key,
	}
	buf, err := req.Enc()
	if err != nil {
		return
	}
	success, respBuf, err := c.client.Conn.SendRequest(req.MessageName(), true, buf)
	if !success {
		err = fmt.Errorf("server refused response")
	}
	if err != nil {
		log.Printf("error when sending %q request: %v", req.MessageName(), err)
		return
	}
	resp := DeleteValueResponse{}
	err = resp.Dec(respBuf)
	if err != nil {
		return
	}
	if resp.Err != "" {
		err = fmt.Errorf(resp.Err)
	}
	return
}

// connect - actually connects to both the SSH agent and the server via their
// respective unix domain sockets
func (c *Client) connect() (err error) {

	socket := os.Getenv("SSH_AUTH_SOCK")
	conn, err := net.Dial("unix", socket)
	if err != nil {
		log.Printf("Failed to open SSH_AUTH_SOCK: %v", err)
		return
	}

	if err != nil {
		log.Printf("Failed to connect to whosesock: %v", err)
		return
	}

	agentClient := agent.NewClient(conn)
	config := &ssh.ClientConfig{
		User: "whose",
		Auth: []ssh.AuthMethod{
			// Use a callback rather than PublicKeys so we only consult the
			// agent once the remote server wants it.
			ssh.PublicKeysCallback(agentClient.Signers),
		},
		// we are not actually talking to another host so let's
		// ignore hostkeys
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	log.Printf("client dialing server on %s", c.sockpath)
	sshc, err := ssh.Dial("unix", c.sockpath, config)
	if err != nil {
		return
	}
	c.client = sshc
	return
}

// Close - closes the ssh connection
func (c *Client) Close() {
	c.client.Close()
}
