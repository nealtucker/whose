package keyhole

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"log"
	"net"
	"os"

	"bitbucket.org/nealtucker/whose/util"
	"golang.org/x/crypto/ssh"
)

// Note: this description is out of date.  keyhole.Server is now just
// an ssh-authenticated key-value store

// whose.Server is a concept that may be generally useful outside
// of this context.  Its job is to manage one or more private keys
// which are passphrase-protected without requiring the user to
// enter their passphrase repeatedly.  The way this works is that
// a daemon is started listening on a unix domain socket which has
// no key material loaded into it.  A client can then connect via
// SSH connection with any identity it wishes and provide an
// encrypted private key it wishes to be associated with this SSH
// identity (based on pubkey fingerprint).  The client may then
// set/unset the passphrase for this key at will, and during any
// time the correct passphrase is set, the client may request crypto
// ops on data buffers.  A private key is associated with a single
// client (by pubkey) and no operations can be performed by any other
// client.
//
// Effectively, this pivots your access to the given key from needing
// to know its passphrase on each use to needing to prove you have the
// same ssh identity as the client that provided the key.  This means
// you can repeatedly use `whose` in client mode without providing the
// passphrase every time.

// Server - a whose server that listens for SSH connections
// on the specified path and will do private key ops on behalf
// of auth'd clients
type Server struct {
	clientStores map[string]*ClientStore
	sockpath     string
	unixListener *net.UnixListener
	quitter      *util.Quitter
}

// NewServer - create a whose server on the given sockpath
func NewServer(sockpath string) (result *Server, err error) {
	s := Server{
		sockpath:     sockpath,
		quitter:      util.NewQuitter(),
		clientStores: make(map[string]*ClientStore),
	}
	result = &s
	return
}

func echoServer(c net.Conn, q *util.Quitter) {
	in := make(chan []byte)
	echoQ := util.NewQuitter()

	go func() {
		defer echoQ.Quit()
		for {
			buf := make([]byte, 512)
			nr, err := c.Read(buf)
			if err != nil {
				log.Printf("read failure on daemon socket: %v", err)
				c.Close()
				echoQ.Quit()
				return
			}

			in <- buf[0:nr]
		}
	}()

	log.Printf("starting echoserver select loop\n")
	for {
		select {
		case <-q.Q:
		case <-echoQ.Q:
			log.Printf("got quit signal in echoserver select loop\n")
			break
		case data := <-in:
			log.Printf("Server got: %q", string(data))
			_, err := c.Write(data)
			if err != nil {
				log.Printf("write failure on daemon socket: %v", err)
				c.Close()
			}
		}
	}
}

// Quit - causes server to exit
func (s *Server) Quit() {
	s.quitter.Quit()
}

func (s *Server) findStore(conn *ssh.ServerConn, create bool) (result *ClientStore, err error) {
	clientID, ok := conn.Permissions.Extensions["whose_client"]
	if !ok {
		err = fmt.Errorf("Store not found")
		return
	}

	log.Printf("Looking up store for clientid %s", clientID)

	result, ok = s.clientStores[clientID]
	if ok {
		return
	}

	if !create {
		err = fmt.Errorf("Store not found")
		return
	}

	newStore, err := NewClientStore()
	if err != nil {
		return
	}
	s.clientStores[clientID] = newStore
	result = newStore
	return
}

func (s *Server) handleStore(conn *ssh.ServerConn, mesg []byte) (result bool, resp []byte) {
	response := StoreValueResponse{}
	defer func() {
		encBytes, encErr := response.Enc()
		if encErr != nil {
			result = false
			return
		}
		result = true
		resp = encBytes
	}()

	store, err := s.findStore(conn, true)
	if err != nil {
		response.Err = err.Error()
		return
	}

	params := StoreValueMessage{}
	err = params.Dec(mesg)
	if err != nil {
		response.Err = err.Error()
		return
	}

	err = store.Add(params.Key, params.Value, params.Expiration)
	if err != nil {
		response.Err = err.Error()
	}

	return
}
func (s *Server) handleGet(conn *ssh.ServerConn, mesg []byte) (result bool, resp []byte) {
	response := GetValueResponse{}
	defer func() {
		encBytes, encErr := response.Enc()
		if encErr != nil {
			log.Printf("Enc failed: %v", encErr)
			result = false
			return
		}
		result = true
		resp = encBytes
	}()

	store, err := s.findStore(conn, true)
	if err != nil {
		log.Printf("findStore failed: %v", err)
		response.Err = err.Error()
		return
	}

	params := GetValueMessage{}
	err = params.Dec(mesg)
	if err != nil {
		response.Err = err.Error()
		log.Printf("Dec failed: %v", err)
		return
	}

	response.Value, err = store.Get(params.Key)
	if err != nil {
		response.Err = err.Error()
	}
	return
}

func (s *Server) handleSSH(conn *ssh.ServerConn, channels <-chan ssh.NewChannel, reqs <-chan *ssh.Request) {
	var whosechan <-chan *ssh.Request
	var err error

	go func() {
		for req := range reqs {
			switch req.Type {
			case "whose_storevalue":
				result, resp := s.handleStore(conn, req.Payload)
				req.Reply(result, resp)
			case "whose_getvalue":
				result, resp := s.handleGet(conn, req.Payload)
				req.Reply(result, resp)
			}
		}
	}()

	go func() {
		for nc := range channels {

			log.Printf("got a new channel request: %#v\n", nc)
			if nc.ChannelType() == "whose" {
				_, whosechan, err = nc.Accept()
				if err != nil {
					log.Printf("error accepting channel request: %v\n", err)
					continue
				}
				go func(reqs <-chan *ssh.Request) {
					for req := range reqs {
						log.Printf("got an unexpected whose channel req: %#v\n", req)
						if req.WantReply {
							log.Printf("sending reply\n")
							err = req.Reply(true, []byte("got your message"))
							log.Printf("reply sent, err=%v", err)
						}
					}
				}(whosechan)
			} else {
				nc.Reject(ssh.UnknownChannelType, "no thanks")
			}
		}
	}()
}

// CloseListener - closes the listening socket and also
// unlinks the unix path
func (s *Server) CloseListener() {
	if s.unixListener == nil {
		// already closed
		return
	}

	// grab the listener and clear it from the server so
	// nobody else closes it (fixme: this whole thing should
	// be atomic)
	ul := s.unixListener
	s.unixListener = nil

	file, err := ul.File()
	if err != nil {
		log.Printf("Server could not close unixListener: %v", err)
		return
	}

	file.Close()
	err = os.Remove(s.sockpath)
	if err != nil {
		log.Printf("Server failed to remove daemon socket %q, err=%v\n", s.sockpath, err)
	}
}

// pubkeyCallback - called when checking pubkeys.  We accept all pubkeys
// and just compartmentalize the kv stores for each one.  All this does is
// stash the key fingerprint in the "whose_client" extension for later use
func pubkeyCallback(conn ssh.ConnMetadata, key ssh.PublicKey) (result *ssh.Permissions, err error) {
	perms := ssh.Permissions{
		Extensions: make(map[string]string),
	}
	perms.Extensions["whose_client"] = ssh.FingerprintSHA256(key)
	return &perms, nil
}

// Serve - listen on the unix socket and handle requests
// Does not exit unless error or s.Quit() is called.
func (s *Server) Serve() error {
	log.Printf("server opening listener on %s", s.sockpath)
	l, err := net.Listen("unix", s.sockpath)
	if err != nil {
		return fmt.Errorf("could not listen on socket: %v", err)
	}

	// Stash a copy of the UnixListener so we can close/remove the socket
	// file later.
	ul, ok := l.(*net.UnixListener)
	if !ok {
		return fmt.Errorf("Could not convert a Listener that was created with type 'unix' to a UnixListener")
	}
	s.unixListener = ul
	defer s.CloseListener()

	go func() {
		for {
			fd, err := l.Accept()
			if err != nil {
				log.Printf("accept failure on daemon socket: %v", err)
				continue
			}

			if err != nil {
				log.Printf("Failed to accept incoming connection (%s)", err)
				continue
			}

			config := ssh.ServerConfig{
				PublicKeyCallback: pubkeyCallback,
			}

			hostkey, err := rsa.GenerateKey(rand.Reader, 2048)
			if err != nil {
				log.Printf("Failed to generate hostkey: %v", err)
				return
			}

			sshSigner, err := ssh.NewSignerFromSigner(hostkey)
			if err != nil {
				log.Printf("could not get ssh signer from rsa private key: %v", err)
				return
			}
			config.AddHostKey(sshSigner)

			// Before use, a handshake must be performed on the incoming net.Conn.
			sshConn, chans, reqs, err := ssh.NewServerConn(fd, &config)
			if err != nil {
				log.Printf("Failed to handshake (%s)", err)
				continue
			}
			_ = chans

			log.Printf("New SSH connection from (%s)", sshConn.ClientVersion())
			// Discard all global out-of-band Requests
			go s.handleSSH(sshConn, chans, reqs)
			//go ssh.DiscardRequests(reqs)
			// Accept all channels
			//go s.handleChannels()

			//go echoServer(fd, s.quitter)
		}
	}()

	select {
	case <-s.quitter.Q:
		// this will happen when we exit regardless, but do as
		// early as possible to signal the other goroutine to
		// exit sooner
		s.CloseListener()
		break
	}
	return nil
}
