package keyhole

import (
	"fmt"
	"log"
	"time"
)

var notFoundErr = fmt.Errorf("Key not found")

type storeEntry struct {
	key        string
	value      string
	expiration time.Time
}
type ClientStore map[string]storeEntry

func NewClientStore() (result *ClientStore, err error) {
	cs := make(ClientStore)
	result = &cs
	return
}

func (cs ClientStore) Add(key, value string, expires time.Time) (err error) {
	cs[key] = storeEntry{
		value:      value,
		key:        key,
		expiration: expires,
	}
	log.Printf("added key %q to store", key)
	return
}

func (cs ClientStore) Get(key string) (result string, err error) {
	log.Printf("getting key %q from store", key)
	entry, ok := cs[key]
	if !ok {
		log.Printf("Key not found")
		err = notFoundErr
		return
	}
	if (entry.expiration != time.Time{}) {
		if entry.expiration.Before(time.Now()) {
			log.Printf("Key expired, deleting and returning not found")
			// maybe present but expired.  delete it
			delete(cs, key)
			err = notFoundErr
			return
		}

		log.Printf("key expires in %v", entry.expiration.Sub(time.Now()))
	}
	result = entry.value
	return
}

func (cs ClientStore) Delete(key string) (err error) {
	log.Printf("Deleting key %q", key)
	delete(cs, key)
	return
}
