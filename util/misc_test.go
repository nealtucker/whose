package util

import (
	"testing"
	"time"
)

func TestDurationParser(t *testing.T) {
	tests := []struct {
		input       string
		shouldErr   bool
		expectedVal time.Duration
	}{
		{input: "23h", expectedVal: 23 * time.Hour},
		{input: "1m", expectedVal: 1 * time.Minute},
		{input: "3000s", expectedVal: 3000 * time.Second},
		{input: "123d", shouldErr: true},
		{input: "123", shouldErr: true},
	}

	for _, tt := range tests {
		testResult, testErr := ParseDuration(tt.input)
		if (testErr != nil) != (tt.shouldErr) {
			t.Errorf("test %q expected shouldErr=%T, actual err: %v", tt.input, tt.shouldErr, testErr)
			continue
		}
		if testErr == nil && tt.expectedVal != testResult {
			t.Errorf("test %q expected result %v, actual %v", tt.input, tt.expectedVal, testResult)
			continue
		}
		if testErr == nil {
			t.Logf("Parsed %q -> %v", tt.input, testResult)
		} else {
			t.Logf("Rejected input %q", tt.input)
		}
	}
}

//fixme: add tests for cases like:
// length specified/unspecified
// password constraints specified/partiallyspecified
