package util

// Quitter - a utility for creating/closing a quit channel
//
// For use in a select loop - NewQuitter returns an object
// with a .Q which is a chan that you can select on.  When
// anyone calls .Quit on the Quitter, the .Q chan is closed
// and the .Done flag is set (handy for auxiliary code that
// may poll rather than use a select loop)
// Example:
// 	select {
// 	case <-quitter.Q:
// 		fmt.Printf("quitting server, closing fd\n")
// 		break
//  ...
// }

// Quitter - a utility for creating/closing a quit channel
type Quitter struct {
	Q    chan struct{}
	Done bool // has this quitter been quitted yet?
}

// NewQuitter - create a quitter
func NewQuitter() *Quitter {
	result := Quitter{
		Q: make(chan struct{}),
	}
	return &result
}

// Quit - quit that quitter
func (q *Quitter) Quit() {
	q.Done = true
	close(q.Q)
}
