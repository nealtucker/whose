package util

import (
	"bytes"
	"fmt"
	"log"
	"os/exec"
	"runtime"
	"strings"
)

type clipboardHandler struct {
	executable string
	args       []string
}

var clipboardPrograms = map[string][]clipboardHandler{
	"darwin": {
		{executable: "pbcopy"},
	},
	"windows": {},
	"linux": {
		{
			executable: "xclip",
			args:       []string{"-in", "-selection", "clipboard"},
		},
		{
			executable: "xsel",
			args:       []string{"-i", "--clipboard"},
		},
	},
}

// autoSelectCopyCommand - determine which copy command
// to use automatically based on the OS
func autoSelectCopyCommand() *clipboardHandler {
	candidates, ok := clipboardPrograms[runtime.GOOS]
	if !ok {
		return nil
	}

	for _, candidate := range candidates {
		_, err := exec.LookPath(candidate.executable)
		if err == nil {
			log.Printf("Found external clipboard handler %q", candidate.executable)
			return &candidate
		}
	}
	log.Printf("found no suitable external clipboard handlers")

	return nil
}

// ErrNoCopyCommand - return from StringToClipboard if no copycmd is specified
// and no command is auto-detected
var ErrNoCopyCommand = fmt.Errorf("no copy command found for OS %q", runtime.GOOS)

// StringToClipboard - run an external program to copy 'data' to the
// OS clipboard.  If copyCmd is given, it is parsed into executable+args
// and executed.  If not, one is automatically selected for some OSes.
func StringToClipboard(data string, copyCmd string) (err error) {

	databuf := bytes.NewReader([]byte(data))
	cmd := exec.Cmd{
		Path:  copyCmd,
		Stdin: databuf,
	}
	if cmd.Path != "" {
		cmd.Path, cmd.Args = splitCopyCommand(cmd.Path)
	}
	if cmd.Path == "" {
		handler := autoSelectCopyCommand()
		if handler != nil {
			cmd.Path = handler.executable
			cmd.Args = handler.args
		}
	}
	if cmd.Path == "" {
		return ErrNoCopyCommand
	}

	cmd.Path, err = exec.LookPath(cmd.Path)
	if err != nil {
		return
	}
	log.Printf("running external command to copy password: %s %s", cmd.Path, strings.Join(cmd.Args, " "))
	err = cmd.Run()
	return
}

// splitCopyCommand - this is a very lame attempt at splitting
// a command into the initial executable plus an array of args.
// if you are doing anything fancy with quoted spaces, this isn't
// going to work.  I'm sorry.
func splitCopyCommand(cmd string) (string, []string) {
	parts := strings.Split(cmd, " ")
	return parts[0], parts[1:]
}
