package util

import "io"

// BellSkipper - io writer filter to remove bells
// This is a workaround for the bell issue documented in
// https://github.com/manifoldco/promptui/issues/49.
type BellSkipper struct {
	out io.WriteCloser
}

// NewBellSkipper - constructor for BellSkipper
func NewBellSkipper(out io.WriteCloser) *BellSkipper {
	return &BellSkipper{
		out: out,
	}
}

// Write implements an io.WriterCloser over os.Stderr, but it skips the terminal
// bell character.
func (bs *BellSkipper) Write(b []byte) (int, error) {
	const charBell = 7 // c.f. readline.CharBell
	if len(b) == 1 && b[0] == charBell {
		return 0, nil
	}
	return bs.out.Write(b)
}

// Close implements an io.WriterCloser over os.Stderr.
func (bs *BellSkipper) Close() error {
	return bs.out.Close()
}
