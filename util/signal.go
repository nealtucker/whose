package util

import (
	"os"
	"os/signal"
)

type signalHandler func(os.Signal)

var handlers map[os.Signal]signalHandler

// SignalMaster - signal listener which forwards
// all signals to registered async handlers
type SignalMaster struct {
	handlers map[os.Signal][]signalHandler
	sigchan  chan os.Signal
}

// NewSignalMaster - constructor for SignalMaster
func NewSignalMaster() *SignalMaster {
	result := SignalMaster{
		handlers: make(map[os.Signal][]signalHandler),
		sigchan:  make(chan os.Signal, 1),
	}

	go result.sigLoop()
	return &result
}

// Add - register a signal handler for the given signal
func (sm *SignalMaster) Add(sig os.Signal, handler func(os.Signal)) {
	existing, ok := sm.handlers[sig]
	if !ok {
		sm.handlers[sig] = []signalHandler{handler}
	} else {
		sm.handlers[sig] = append(existing, handler)
	}
	signal.Notify(sm.sigchan, sig)
}

// sigLoop - signal goroutine which runs forever
func (sm *SignalMaster) sigLoop() {
	for {
		s := <-sm.sigchan
		handlers, ok := sm.handlers[s]
		if ok {
			for _, h := range handlers {
				go h(s)
			}
		}
	}
}
