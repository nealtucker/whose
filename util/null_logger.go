package util

// NullLoggerImpl - implementation of a logger that logs nothing
type NullLoggerImpl struct{}

func (nli *NullLoggerImpl) Write(p []byte) (n int, err error) {
	return
}

// NullLogger - return a logger which logs nothing
func NullLogger() *NullLoggerImpl {
	return &NullLoggerImpl{}
}
