package util

import (
	"fmt"
	"regexp"
	"strconv"
	"time"
)

var durationExpr = regexp.MustCompile("^(^[0-9]+)((s|m|h))$")
var multipliers = map[string]time.Duration{
	"s": time.Second,
	"m": time.Minute,
	"h": time.Hour,
}

// ParseDuration - parse a duration string of the form "123h" where
// the units can be either "s", "m", or "h" for seconds, minutes, hours,
// respectively.  Intolerant of sloppiness.
func ParseDuration(dur string) (result time.Duration, err error) {
	parts := durationExpr.FindStringSubmatch(dur)
	if parts == nil {
		err = fmt.Errorf("invalid duration string: %q", dur)
		return
	}

	num, err := strconv.Atoi(parts[1])
	if err != nil {
		err = fmt.Errorf("invalid duration string: %q", dur)
		return
	}

	result = time.Duration(num) * multipliers[parts[2]]
	return
}
