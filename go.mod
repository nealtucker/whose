module bitbucket.org/nealtucker/whose

go 1.14

require (
	github.com/manifoldco/promptui v0.8.0
	github.com/mattn/go-colorable v0.1.7 // indirect
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	golang.org/x/sys v0.0.0-20200918174421-af09f7315aff // indirect
)
